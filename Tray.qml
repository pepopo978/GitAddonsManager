/* Copyright 2018 WobLight
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Qt.labs.platform 1.0
import GitAddonsManager.engine 1.0


SystemTrayIcon {
    visible: true
    iconName: availableUpdates > 0 ? "update-high" : "update-none"
    onActivated: {
        window.show()
        window.raise()
        window.requestActivate()
    }
    menu: Menu {
        visible: false
        MenuItem {
            text: qsTr("Check for updates")
            onTriggered: Engine.scanForAddons()
            enabled: Engine.status == Engine.Ready
        }
        MenuItem {
            text: qsTr("Update all")
            onTriggered: updateAll()
            enabled: availableUpdates > 0
        }
        MenuSeparator {}
        MenuItem {
            text: qsTr("Close")
            onTriggered: Qt.quit()
        }
    }
}
